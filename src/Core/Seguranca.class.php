<?php
    namespace Havit\Core;
    /**
     * Description of seguranca
     *
     * @author Gustavo
     */
    class Seguranca
    {

        /**
         * Pega todos os par?metros de GET e POST
         */
        public static function QueryProtect()
        {
            foreach ($_POST as $key => &$POST) {
                if (is_array($POST)) {
                    foreach ($POST as &$p) {
                        if (!(substr($key, 0, 3) == "tx_")) {
                            $p = self::StringProtect($p);
                        }
                        if (substr($key, 0, 3) == "va_") {
                            $p = str_replace("R$ ", "", $p);
                            $p = str_replace(".", "", $p);
                            $p = str_replace(",", ".", $p);
                            $p = str_replace(",", ".", $p);
                        }
                        if (substr($key, 0, 3) == "is_") {
                            if ($p == "1") {
                                $p = 1;
                            } else {
                                $p = 0;
                            }
                        }
                    }
                } else {
                    if (!(substr($key, 0, 3) == "tx_")) {
                        $POST = self::StringProtect($POST);
                    }
                    if (substr($key, 0, 3) == "va_") {
                        $POST = str_replace("R$ ", "", $POST);
                        $POST = str_replace(".", "", $POST);
                        $POST = str_replace(",", ".", $POST);
                        $POST = str_replace(",", ".", $POST);
                    }
                    if (substr($key, 0, 3) == "is_") {
                        if ($POST == "1") {
                            $POST = 1;
                        } else {
                            $POST = 0;
                        }
                    }
                }
            }

            foreach ($_GET as &$GET) {
                if (is_array($GET)) {
                    foreach ($GET as &$g) {
                        if (is_array($GET)) {
                            $g = self::StringProtect($g);
                        } else {
                            $g = self::StringProtect($g);
                        }
                    }
                } else {
                    $GET = self::StringProtect($GET);
                }
            }
        }

        /**
         * Aplica seguran?a na STRING.
         *
         * @param $strProtect
         *
         * @return string
         */
        public static function StringProtect($strProtect)
        {

            if (!is_array($strProtect)) {
                $strProtect = ((get_magic_quotes_gpc()) ? $strProtect : addslashes($strProtect));
            }

            /*
            http://www.php.net/manual/pt_BR/function.htmlentities.php  - Transforma por ex. ? = &ecute  - Funciona em algumas vers?es do PHP.
            */
            //$strProtect = htmlentities($strProtect);

            /*
               http://www.php.net/manual/pt_BR/function.mysql-real-escape-string.php - Esta fun??o ir? escapar os caracteres especiais em unescaped_string, levando em conta o atual conjunto de caracteres da conex?o, assim ? seguro coloca-la em mysql_query().
               Somente se For MySQL
            */

            //$strProtect = mysql_real_escape_string($strProtect);

            return $strProtect;
        }


        public static function Encode($string)
        {
            // 64 Chars.
            $base = "u1Astx8CxggMn6OpUt7WyjF4G2cZvb91klthf4RhSiRIIwH3o05mDrEaJdT0B/+0";
            //a grande diferenca esta aqui, definimos uma string de 64 caracteres, que pode ser personalizada da maneira que quiserem, desde que sejam sempre 64 caracteres
            $encoded = "";
            $b64_str = base64_encode($string);
            $i = 0;
            $j = 0;
            while ($i < strlen($b64_str)) {
                if ($i == 64)
                    break;
                $k[$i] = $b64_str[$i] . $base[$i];
                $encoded .= $k[$i];
                $i++;
            }

            return $encoded;
        }

        public static function Decode($string)
        {
            $i = 0;
            $j = 0;
            while ($i < strlen($string)) {
                $k[$i] = $string[$j];
                $decoded .= $k[$i];
                $i++;
                $j = $j + 2;
            }
            $decoded = base64_decode($decoded);

            return $decoded;
        }

    }

?>
