<?php
    /**
     * Created by PhpStorm.
     * User: Gustavo
     */

    namespace Havit\Core;


    use Havit\Core\Helpers\SQL\SQL;

    class DB
    {
        /**
         * @param $table
         *
         * @return SQL;
         */
        public static function table($table)
        {
            $sql = new SQL();

            return $sql->table($table);
        }
    }