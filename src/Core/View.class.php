<?php

    /**
     * User: Gustavo
     * Date: 01/11/13
     * Time: 18:29
     */

    namespace Havit\Core;

    use Havit\Core\Utils\StringUtils;
    use Havit\Helpers\GUI;

    class View
    {


        public static function Renderizar($view, $pasta = null)
        {
            return ["view" => $view, "pasta" => $pasta, "tipo" => "renderizar"];
        }

        public static function RenderizarComoPDF($view, $pasta = null)
        {
            return ["view" => $view, "pasta" => $pasta, "tipo" => "pdf"];
        }


        public static function RedirecionarPara($url, $tipoMsg = null, $msg = null)
        {
            if (isset($tipoMsg) && isset($msg)) {
                if (StringUtils::contains("?", $url)) {
                    $url .= "&stpmsg=" . $tipoMsg . "&smsg=" . $msg;
                } else {
                    $url .= "?stpmsg=" . $tipoMsg . "&smsg=" . $msg;
                }
            }

            return ["view" => $url, "pasta" => null, "tipo" => "redirecionar"];
        }

        public static function StopAndRedirect($view)
        {
            echo "<script>window.location.replace('" . $view . "');</script>";
            header("location: " . $view . "");
            exit();
        }

        /**
         * @param $template
         */
        function setTemplate($template)
        {
            if (!isset($GLOBALS['_layout'])) {
                $GLOBALS['_layout'] = $template;
            }
        }

        /**
         * @param $titulo
         */
        function setTitulo($titulo)
        {
            if (!isset($GLOBALS['_titulo'])) {
                $GLOBALS['_titulo'] = $titulo;
            }
        }


        /**
         * @param $descricao
         */
        function setDescricao($descricao)
        {
            if (!isset($GLOBALS['_descricao'])) {
                $GLOBALS['_descricao'] = $descricao;
            }
        }

        /**
         * @param $idMenu
         */
        function setMenu($idMenu)
        {
            GUI::selecionarMenuAtivo(strtolower($idMenu), "active");
        }


        /**
         * @param        $controllerNome  // Nome do Controller
         * @param        $funcaoNome      // Nome da Fun??o
         * @param array  $colunasEsconder // Mostrar a Coluna ID - TRUE|FALSE
         * @param string $idNome          // Id da DIV que ir? mostrar a tabela
         */
        function Grid($controllerNome, $funcaoNome, $colunasEsconder = [], $idNome = "tabelaPrincipal")
        {

            $grid = "  var grid = new Datatable();  grid.init({
               src: $('#" . $idNome . "'),
    \"sPaginationType\": \"bootstrap\",
    dataTable: { // here you can define a typical datatable settings from http://datatables.net/usage/options

                \"bStateSave\": true, // save datatable state(pagination, sort, etc) in cookie.

                \"lengthMenu\": [
                    [10, 20, 50, 100, 150, -1],
                    [10, 20, 50, 100, 150, \"Todos\"] // change per page values here
                ],
                \"pageLength\": 10, // default record count per page
                \"processing\": true,
                \"serverSide\": true,
                \"ajax\": {
                    \"url\":  \"../../" . $controllerNome . "/" . $funcaoNome . "\"
                },
                \"order\": [
                    [1, \"asc\"]
                ], // set first column as a default sort by asc
                    \"aoColumnDefs\": [
        {
            'bVisible': false,
            'bSortable': false,
            'aTargets': [" . implode(', ', array_values($colunasEsconder)) . "]
        }
    ],
            }

});

";
            GUI::adicionaJS($grid);
        }


        /**
         * @param string $idNome
         */
        function GridEstatico($idNome = "tabelaPrincipal")
        {

            $grid = "oTable = $('#" . $idNome . "').dataTable({
    \"sPaginationType\": \"bootstrap\",
    \"oLanguage\": {
      \"sLengthMenu\": \"_MENU_ registros por p?gina\",
            \"sProcessing\": \"Processando...\",
            \"sZeroRecords\": \"Nenhum registro encontrado\",
            \"sEmptyTable\": \"Sem registros dispon&iacute;veis\",
            \"sLoadingRecords\": \"Carregando...\",
            \"sInfo\": \"Mostrando _START_ at? _END_ de _TOTAL_ registros\",
            \"sInfoEmpty\": \"Mostrando 0 at? 0 de 0 registros\",
            \"sInfoFiltered\": \"(_MAX_ no total)\",
            \"sInfoPostFix\": \"\",
            \"sInfoThousands\": \",\",
            \"sSearch\": \"Procurar\",
            \"fnInfoCallback\": null
        },

    //Salva estado da grid
    \"iDisplayLength\": 10,
    // \"bStateSave\": true,
    \"bProcessing\": true,
     \"aaSorting\": []

});

jQuery('#tabelaPrincipal_wrapper .dataTables_filter input').addClass(\"m-wrap medium\"); // modify table search input
jQuery('#tabelaPrincipal_wrapper .dataTables_length select').addClass(\"m-wrap xsmall\"); // modify table per page dropdown
";
            GUI::adicionaJS($grid);
        }
        function action($view, $controller, $area, $parametros)
        {
            $url = BASE_PATH . "$area/$controller/$view/$parametros";
            return file_get_contents($url);


        }
    }