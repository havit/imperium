<?php
namespace Havit\Core;


/**
 * User: Gustavo
 * Date: 12/04/13
 * Time: 12:58
 */
abstract class Controller
{
    protected $_controller;
    protected $_action;
    protected $_template;
    public $view;
    public $renderizar;

    function __construct($area, $controller, $acao)
    {
        $this->view = new View();
        $this->_template = new Template($area, $controller, $acao);
        $this->renderizar = 1;
        $this->id = null;
        if (isset($_GET['id'])) {
            $this->id = $_GET['id'];
        }
        if (isset($_POST['id'])) {
            $this->id = $_POST['id'];
        }
    }

    function set($variavel, $valor)
    {
        $this->_template->set($variavel, $valor);
    }

    function __destruct()
    {
        if ($this->renderizar) {
            $this->_template->renderizar($this->view);
        }
    }



}