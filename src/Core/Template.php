<?php

namespace Havit\Core;

use Havit\Helpers\Swal;

/**
 * User: Iuri Gustavo
 * Date: 27/07/13
 * Time: 09:08
 */
class Template
{
    protected $variaveis = [];
    protected $_controller;
    protected $_acao;
    protected $_area;
    protected $_pagina;

    function __construct($area, $controller, $acao)
    {
        $this->_area = $area;
        $this->_controller = $controller;
        $this->_acao = $acao;
    }

    function set($nome, $valor)
    {
        $this->variaveis[$nome] = $valor;
    }

    function preencheVar($var)
    {
        foreach($var as $key => $value) {
           $this->$key = $value;
        }
    }

    function renderizar($view)
    {

        if (isset($_GET['stpmsg']) && isset($_GET['smsg'])) {
            Swal::mensagemTipo($_GET['stpmsg'], $_GET['smsg']);
        }

        if (isset($GLOBALS['_pagina'])) {
            $this->_pagina = $GLOBALS['_pagina'];
        } else {
            $this->_pagina = "Index";
        }
        $this->preencheVar($view);
        extract($this->variaveis);
        $html = new Html();
        if (is_array($this->_pagina)) {
            if ($this->_pagina['tipo'] == "redirecionar") {
                echo "<script>window.location.replace('" . $this->_pagina['view'] . "');</script>";
                header("location: " . $this->_pagina['view'] . "");
                exit();
            } else {
                if ($this->_pagina['pasta'] == NULL)
                    $localArquivo = PROJECT_ROOT . DS . 'app' . DS . 'modules' . DS . $this->_area . DS . 'Views' . DS . $this->_controller . DS . $this->_pagina['view'] . '.php';
                else {
                    $localArquivo = PROJECT_ROOT . DS . 'app' . DS . 'modules' . DS . $this->_area . DS . 'Views' . DS . $this->_pagina['pasta'] . DS . $this->_pagina['view'] . '.php';
                }
            }
        } else {
            $localArquivo = PROJECT_ROOT . DS . 'app' . DS . 'modules' . DS . $this->_area . DS . 'Views' . DS . $this->_controller . DS . $this->_pagina . '.php';
        }

        if (file_exists($localArquivo)) {
            ob_start();
            include($localArquivo);
            $GLOBALS['_conteudo'] = ob_get_contents();
            echo $GLOBALS['_conteudo'];
            ob_end_clean();

            if ($this->_pagina['tipo'] == "pdf") {
                //                    error_reporting(E_ALL);
                //                    ini_set('display_errors', 'Off');
                //                    $mpdf = new mPDF('c');
                //                    $mpdf->ignore_invalid_utf8 = true;
                //                    $bootstrap = file_get_contents('../public/admin/assets/plugins/bootstrap/css/bootstrap.min.css');
                //                    $uniform = file_get_contents('../public/admin/assets/plugins/uniform/css/uniform.default.css');
                //                    $cssprint = file_get_contents('../public/admin/assets/css/pages/invoice.css');
                //                    $css1 = file_get_contents('../public/admin/assets/css/style-metronic.css');
                //                    $css2 = file_get_contents('../public/admin/assets/css/style.css');
                //                    $css3 = file_get_contents('../public/admin/assets/css/style-responsive.css');
                //                    $css4 = file_get_contents('../public/admin/assets/css/plugins.css');
                //                    $css5 = file_get_contents('../public/admin/assets/css/themes/light.css');
                //
                //                    $mpdf->WriteHTML($bootstrap,1);
                //                    $mpdf->WriteHTML($uniform,1);
                //                    $mpdf->WriteHTML($cssprint,1);
                //                    $mpdf->WriteHTML($css1,1);
                //                    $mpdf->WriteHTML($css2,1);
                //                    $mpdf->WriteHTML($css3,1);
                //                    $mpdf->WriteHTML($css4,1);
                //                    $mpdf->WriteHTML($css5,1);
                //                    $mpdf->WriteHTML(utf8_encode($GLOBALS['_conteudo']));
                //                    $mpdf->Output();
                exit;
            } else {

                if (isset($GLOBALS['_layout'])) {
                    if (file_exists(PROJECT_ROOT . DS . 'app' . DS . 'templates' . DS . $GLOBALS['_layout'] . '.class.php')) {
                        include(PROJECT_ROOT . DS . 'app' . DS . 'templates' . DS . $GLOBALS['_layout'] . '.class.php');
                        $html->Template = new $GLOBALS['_layout']();

                    }
                    include(PROJECT_ROOT . DS . 'app' . DS . 'templates' . DS . $GLOBALS['_layout'] . '.php');
                } else {
                    echo $GLOBALS['_conteudo'];
                }


            }
        } else {
            $GLOBALS['_conteudo'] = "";
            if (isset($GLOBALS['_layout'])) {
                include(PROJECT_ROOT . DS . 'app' . DS . 'templates' . DS . $GLOBALS['_layout'] . '.php');
            }
        }
    }
}

