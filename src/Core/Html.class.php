<?php
    /**
     * User: Iuri Gustavo
     * Date: 27/07/13
     * Time: 17:27
     *
     */
    namespace Havit\Core;

    use Havit\Core\Template\Layout;
    use Havit\Core\Utils\StringUtils;

    class Html
    {

        /** @var $Layout Layout */
        public $Layout;
        public $Template;

        function __construct()
        {
            $this->Layout = new Layout();
        }


        public static function ActionLink($action, $controller = null, $area = null)
        {

            $cur_url = str_replace(BASE_PATH, "", CURRENT_URL);
            $cur_url = str_replace(_AREA, "", $cur_url);
            $expld = explode("/", $cur_url);
            if ($controller != null) {
                if (sizeof($expld) == 3)
                    return $area == null ? "../../" . $controller . "/" . $action . "/" : "../../" . $area . "/" . $controller . "/" . $action . "/";
                else
                    return $area == null ? "../../" . $controller . "/" . $action . "/" : "../../../" . $area . "/" . $controller . "/" . $action . "/";
            } else {
                return "../" . $action . "/";
            }

        }

        public static function RedirecionarPara($url, $tipoMsg = null, $msg = null)
        {
            if (isset($tipoMsg) && isset($msg)) {
                if (StringUtils::contains("?", $url)) {
                    $url .= "&stpmsg=" . $tipoMsg . "&smsg=" . $msg;
                } else {
                    $url .= "?stpmsg=" . $tipoMsg . "&smsg=" . $msg;
                }
            }

            header("location:$url");

        }


    }

