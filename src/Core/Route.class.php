<?php
/**
 * User: iurigustavo
 */

namespace Havit\Core;


class Route
{

    public static function get($pagina, $funcao)
    {
        if (isset($GLOBALS['url'])) {
            $len = strlen($pagina);
            $substr = "/" . substr($GLOBALS['url'], 0, $len - 1);
            if ($pagina == $substr) $funcao();
        }

    }

    public static function isArea($area, $funcao)
    {
        if (isset($GLOBALS['area'])) {
            if ($GLOBALS['area'] == $area) $funcao();
        }

    }

}