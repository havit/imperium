<?php
namespace Havit\Core;

use Havit\Core\Helpers\SQL\SQL;
use Havit\Core\Utils\StringUtils;


class Model
{
    protected static $database;
    protected static $table;
    protected static $primaryKey = ["id"];
    protected static $columns = [];
    /** @var  SQL */
    private static $conexao;

    /**
     * @param $id
     *
     * @return bool|object
     */
    public static function find($id)
    {
        $c = new SQL();
        $c->table(static::$table);
        if (is_array($id)) {
            $x = 0;
            foreach (static::$primaryKey as $ida) {
                $c->whereEq($ida, $id[$x]);
                $x++;
            }
        } else {
            $c->whereEq(static::$primaryKey[0], $id);
        }

        return $c->find();
    }

    public static function all()
    {

        $c = new  SQL();
        $c->table(static::$table);

        return $c->get();
    }

    /**
     * @param $campos
     * @return SQL
     */
    public static function select($campos)
    {
        if (!isset(self::$conexao)) {
            self::$conexao = new SQL();
            self::$conexao->table(static::$table);
        } else {
            self::$conexao->table(static::$table);
        }

        return self::$conexao->select($campos);

    }

    public static function whereEq($campo, $valor)
    {
        return self::where($campo, "=", $valor);
    }

    public static function joinLeft($tabela, $condicao, $colunas = null)
    {

        if (!isset(self::$conexao)) {
            self::$conexao = new SQL();
            self::$conexao->table(static::$table);
        } else {
            self::$conexao->table(static::$table);
        }

        return self::$conexao->joinLeft($tabela, $condicao, $colunas);
    }

    public static function joinRight($tabela, $condicao, $colunas = null)
    {

        if (!isset(self::$conexao)) {
            self::$conexao = new SQL();
            self::$conexao->table(static::$table);
        } else {
            self::$conexao->table(static::$table);
        }

        return self::$conexao->joinRight($tabela, $condicao, $colunas);
    }


    /**
     * @param        $campo
     * @param        $operador
     * @param        $valor
     * @param string $conjuncao
     *
     * @return SQL
     */
    public static function where($campo, $operador, $valor, $conjuncao = 'and')
    {
        if (!isset(self::$conexao)) {
            self::$conexao = new SQL();
            self::$conexao->table(static::$table);
        } else {
            self::$conexao->table(static::$table);
        }
        return self::$conexao->where($campo, $operador, $valor, $conjuncao);
    }

    /**
     * @param null $where
     * @param null $parametro
     *
     * @return SQL
     */
    public static function whereRaw($where = NULL, $parametro = NULL)
    {
        if (!isset(self::$conexao)) {
            self::$conexao = new SQL();
            self::$conexao->table(static::$table);

            return self::$conexao->whereRaw($where, $parametro);
        } else {
            self::$conexao->table(static::$table);

            return self::$conexao->whereRaw($where, $parametro);
        }
    }


    public static function create($array)
    {

        $c = Conexao::getInstance();
        $tbl_nome = static::class;
        /** @var Model $tbl */
        $tbl = new $tbl_nome;
        foreach ($array as $key => $var) {
            $existe = FALSE;
            foreach (static::$columns as $col) {
                if ($key == $col) {
                    $existe = TRUE;
                }
            }
            if (!$existe) {
                unset($array[$key]);
            }
        }

        $sql = "INSERT INTO " . $tbl::$table . " " . "(" . implode(",", array_keys($array)) . ")";
        $qtdColunas = sizeof($array);
        $valores = [];
        for ($x = 1; $x <= $qtdColunas; $x++) {
            $valores[] = "?";
        }
        $sql .= " VALUES (" . implode(",", $valores) . ") ";

        $c->preparaStatement($sql);
        foreach ($array as $key => $value) {
            $c->adicionaParametros($value);
        }

        if ($c->executaStatement()) {
            foreach ($tbl::$primaryKey as $field) {
                $tbl->__set($field, $c->last_id);
            }

            return $tbl;
        }

        return $tbl;

    }

    public static function destroy($vl)
    {
        $c = new SQL();
        $c->table(static::$table);
        if (is_array($vl)) {
            $x = 0;
            foreach (static::$primaryKey as $ida) {
                $c->where($ida, "=", $vl[$x]);
                $x++;
            }
        } else {
            $c->where(static::$primaryKey[0], "=", $vl);
        }

        return $c->delete();

    }

    public function findMe()
    {
        $c = new SQL();
        $c->table($this::$table);
        $x = 0;
        foreach ($this::$primaryKey as $ida) {
            $c->whereEq($ida, $this->__get($ida));
            $x++;
        }

        /** @var Model $resultado */
        $resultado = $c->find();
        if ($resultado)
            $this->fill($resultado->getArray());
        else
            return FALSE;

        return $this;
    }

    public function findBy($coluna,$valor)
    {
        $c = new SQL();
        $c->table($this::$table);
        $c->whereEq($coluna, $valor);

        /** @var Model $resultado */
        $resultado = $c->find();
        if ($resultado)
            $this->fill($resultado->getArray());
        else
            return FALSE;


        return $this;
    }

    public
    function __get($campo)
    {
        return $this->$campo;
    }

    public
    function __set($campo, $valor)
    {

        if (!call_user_func([$this, StringUtils::camelCase("set" . ucfirst($campo))], $valor))
            $this->$campo = $valor;
    }

    public
    function fill($array = [])
    {
        foreach ($array as $key => $var) {
            if (StringUtils::verify($var) != NULL) {
                foreach ($this::$columns as $col) {
                    if ($col == $key) {
                        $this->__set($col, $var);
                    }
                }
            }
        }
    }

    public
    function getArray()
    {
        $array = [];
        foreach (static::$columns as $colunas) {
            $vl = $this->__get($colunas);

            if (isset($vl)) {
                $array[$colunas] = $this->__get($colunas);
            }
        }
        return $array;
    }

    public
    function save()
    {
        $array = [];
        $isNewRecord = TRUE;
        foreach ($this::$columns as $colunas) {
            $vl = $this->__get($colunas);
            if (isset($vl)) {
                $array[$colunas] = $this->__get($colunas);
            }
        }
        if (sizeof($this::$primaryKey) == 1) {
            foreach ($this::$primaryKey as $ida) {
                $vl = $this->__get($ida);
                if (isset($vl))
                    $isNewRecord = FALSE;
            }
        } else {
            $countValorPreenchido = 0;
            foreach ($this::$primaryKey as $ida) {
                $vl = $this->__get($ida);
                if (isset($vl))
                    $countValorPreenchido++;
            }
            if ($countValorPreenchido == sizeof($this::$primaryKey)) {
                $consulta = DB::table($this::$table);
                foreach ($this::$primaryKey as $ida) {
                    $consulta->where($ida, "=", $this->__get($ida));
                }
                if ($consulta->exists())
                    $isNewRecord = FALSE;
            }
        }
        if (sizeof($array) == 0) {
            return FALSE;
        } else {
            if ($isNewRecord) {
                $c = Conexao::getInstance();
                foreach ($array as $key => $var) {
                    $existe = FALSE;
                    foreach (static::$columns as $col) {
                        if ($key == $col) {
                            $existe = TRUE;
                        }
                    }
                    if (!$existe) {
                        unset($array[$key]);
                    }
                }

                $sql = "INSERT INTO " . $this::$table . " " . "(" . implode(",", array_keys($array)) . ")";
                $qtdColunas = sizeof($array);
                $valores = [];
                for ($x = 1; $x <= $qtdColunas; $x++) {
                    $valores[] = "?";
                }
                $sql .= " VALUES (" . implode(",", $valores) . ") ";
                $c->preparaStatement($sql);
                foreach ($array as $key => $value) {
                    $c->adicionaParametros($value);
                }


                if ($c->executaStatement()) {
                    foreach ($this::$primaryKey as $field) {
                        $this->__set($field, $c->last_id);
                    }

                    return TRUE;
                }

                return FALSE;

            } else {
                return $this->update();
            }
        }
    }

    private
    function update()
    {
        $c = Conexao::getInstance();
        $sql = "UPDATE " . $this::$table;
        $campos = [];
        $valores = [];
        $where = [];

        $good = array_diff($this::$columns, $this::$primaryKey);
        foreach ($good as $coluna) {
            $valor = $this->__get($coluna);
            if (isset($valor)) {
                $campos[] = $coluna . "=?";
                $valores[] = $this->__get($coluna);
            }
        }

        // Monta Where
        foreach ($this::$primaryKey as $ida) {
            $where[] = $ida . "=?";
            $valores[] = $this->__get($ida);
        }
        $sql .= " SET " . implode(",", $campos);
        $sql .= " WHERE " . implode(" AND ", $where);
        $c->preparaStatement($sql);
        foreach ($valores as $v) {
            $c->adicionaParametros($v);
        }
        if ($c->executaStatement()) {
            return TRUE;
        }

        return FALSE;
    }


}