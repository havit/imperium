<?php
    namespace PFBC\Element;

    class ButtonBack extends \PFBC\Element
    {
        protected $_attributes = ["type" => "button", "value" => "Submit"];
        protected $icon;

        public function __construct($label = "Voltar", $type = "button", array $properties = ["onclick" => "history.go(-1);"])
        {
            if (!is_array($properties))
                $properties = [];

            if (!empty($type))
                $properties["type"] = $type;

            $class = "btn";
            if (empty($type) || $type == "submit")
                $class .= " blue";

            if (!empty($properties["class"]))
                $properties["class"] .= " " . $class;
            else
                $properties["class"] = $class;

            if (empty($properties["value"]))
                $properties["value"] = $label;

            parent::__construct("", "", $properties);
        }
    }
