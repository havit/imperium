<?php
/**
 * Created by PhpStorm.
 * User: Gustavo
 */

namespace Havit\Core\Utils;


class Diversos
{
    public static function geraChave()
    {
        $character_set_array = [];
        $character_set_array[] = ['count' => 6, 'characters' => 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'];
        $character_set_array[] = ['count' => 4, 'characters' => '0123456789'];
        $temp_array = [];
        foreach ($character_set_array as $character_set) {
            for ($i = 0; $i < $character_set['count']; $i++) {
                $temp_array[] = $character_set['characters'][rand(0, strlen($character_set['characters']) - 1)];
            }
        }
        shuffle($temp_array);

        return implode('', $temp_array);
    }


    public static function validaCPF($cpf = null)
    {
        if (strlen($cpf) > 14) {
            return false;
        }
        // determina um valor inicial para o digito $d1 e $d2
        // pra manter o respeito ;)
        $d1 = 0;
        $d2 = 0;
        // remove tudo que n�o seja n�mero
        $cpf = preg_replace("/[^0-9]/", "", $cpf);
        // lista de cpf inv�lidos que ser�o ignorados
        $ignore_list = ['00000000000', '01234567890', '11111111111', '22222222222', '33333333333', '44444444444', '55555555555', '66666666666', '77777777777', '88888888888', '99999999999'];
        // se o tamanho da string for dirente de 11 ou estiver
        // na lista de cpf ignorados j� retorna false
        if (strlen($cpf) != 11 || in_array($cpf, $ignore_list)) {
            return false;
        } else {
            // inicia o processo para achar o primeiro
            // n�mero verificador usando os primeiros 9 d�gitos
            for ($i = 0; $i < 9; $i++) {
                // inicialmente $d1 vale zero e � somando.
                // O loop passa por todos os 9 d�gitos iniciais
                $d1 += $cpf[$i] * (10 - $i);
            }
            // acha o resto da divis�o da soma acima por 11
            $r1 = $d1 % 11;
            // se $r1 maior que 1 retorna 11 menos $r1 se n�o
            // retona o valor zero para $d1
            $d1 = ($r1 > 1) ? (11 - $r1) : 0;
            // inicia o processo para achar o segundo
            // n�mero verificador usando os primeiros 9 d�gitos
            for ($i = 0; $i < 9; $i++) {
                // inicialmente $d2 vale zero e � somando.
                // O loop passa por todos os 9 d�gitos iniciais
                $d2 += $cpf[$i] * (11 - $i);
            }
            // $r2 ser� o resto da soma do cpf mais $d1 vezes 2
            // dividido por 11
            $r2 = ($d2 + ($d1 * 2)) % 11;
            // se $r2 mair que 1 retorna 11 menos $r2 se n�o
            // retorna o valor zeroa para $d2
            $d2 = ($r2 > 1) ? (11 - $r2) : 0;
            // retona true se os dois �ltimos d�gitos do cpf
            // forem igual a concatena��o de $d1 e $d2 e se n�o
            // deve retornar false.
            return (substr($cpf, -2) == $d1 . $d2) ? true : false;
        }
    }

    //needs "php_curl" to be enabled (+php_openssl)
    public static function get_remote_data($url, $use_FOLLOWLOCATION = true, $post_paramtrs = false, $from_mobile = false)
    {
        $c = curl_init();
        curl_setopt($c, CURLOPT_URL, $url);
        curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
        if ($post_paramtrs) {
            curl_setopt($c, CURLOPT_POST, TRUE);
            curl_setopt($c, CURLOPT_POSTFIELDS, "var1=bla&" . $post_paramtrs);
        }
        curl_setopt($c, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($c, CURLOPT_USERAGENT, ($from_mobile) ? "Mozilla/5.0 (iPhone; U; CPU like Mac OS X; en) AppleWebKit/420+ (KHTML, like Gecko) Version/3.0 Mobile/1C25 Safari/419.3" : "Mozilla/5.0 (Windows NT 6.1; rv:33.0) Gecko/20100101 Firefox/33.0");
        curl_setopt($c, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($c, CURLOPT_MAXREDIRS, 10);
        curl_setopt($c, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($c, CURLOPT_CONNECTTIMEOUT, 9);
        curl_setopt($c, CURLOPT_TIMEOUT, 60);
        curl_setopt($c, CURLOPT_HEADER, true);
        curl_setopt($c, CURLOPT_REFERER, $url);
        curl_setopt($c, CURLOPT_ENCODING, 'gzip,deflate');
        curl_setopt($c, CURLOPT_AUTOREFERER, true);
        $header[0] = "Accept: text/xml,application/xml,application/xhtml+xml," . "text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5";
        $header[] = "Cache-Control: max-age=0";
        $header[] = "Connection: keep-alive";
        $header[] = "Keep-Alive: 300";
        $header[] = "Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7";
        $header[] = "Accept-Language: en-us,en;q=0.5";
        $header[] = "Pragma: ";
        curl_setopt($c, CURLOPT_HTTPHEADER, $header);

        //==========EXECUTE=========
        $got_html = curl_exec($c);
        $status = curl_getinfo($c);
        curl_close($c);
        //if TURNED OFF "FOLLOWLOCATION"
        if (!$use_FOLLOWLOCATION) {
            if ($status['http_code'] == 200) {
                return $got_html;
            } else {
                if ($status['http_code'] == 301 || $status['http_code'] == 302) {
                    list($header) = explode("\r\n\r\n", $got_html, 2);
                    preg_match("/(Location:|URI:)[^(\n)]*/", $header, $matches);
                    $url = trim(str_replace($matches[1], "", $matches[0]));
                    $url_parsed = parse_url($url);
                    return (isset($url_parsed)) ? Diversos::get_remote_data($url, $use_FOLLOWLOCATION, $post_paramtrs, $from_mobile) : "ERRORCODE11:<br/>can't catch redirected url. LAST RESPONSE:<br/><br/>$got_html";
                } else {
                    $oline = '';
                    foreach ($status as $key => $eline) {
                        $oline .= '[' . $key . ']' . $eline . ' ';
                    }
                    $line = $oline . " <br/> " . $url . "<br/>-----------------<br/>";
                    return "ERRORCODE13:<br/>$line";
                }
            }
        }
    }
}