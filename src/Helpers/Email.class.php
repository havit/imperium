<?php
    namespace helpers;

    /**
     * Created by PhpStorm.
     * User: user
     */
    class Email
    {


        public static function  EnviaEmail($nomeDestinatario, $emailDestinatario, $msg, $assunto)
        {

            $to_send = new \PHPMailer(TRUE);
            $to_send->IsSMTP();
            $to_send->Host = $GLOBALS['email']['host'];
            $to_send->SMTPAuth = TRUE;
            $to_send->Username = $GLOBALS['email']['usuario'];
            $to_send->Password = $GLOBALS['email']['senha'];
            $to_send->From = $GLOBALS['email']['usuario'];
            $to_send->Sender = $GLOBALS['email']['usuario'];
            $to_send->FromName = $GLOBALS['email']['nome'];
            $to_send->AddAddress($emailDestinatario, $nomeDestinatario);
            $to_send->Subject = $assunto;
            $to_send->IsHTML(TRUE);
            $to_send->Body = $msg;
            $to_send->Porta = 587;

            $to_send->IsHTML(TRUE);
            $enviado = $to_send->Send();
            if ($enviado) {
                return TRUE;
            } else {
                echo $to_send->ErrorInfo;

                return FALSE;
            }
        }


    }