<?php
    /**
     * Created by PhpStorm.
     * User: Gustavo
     */

    namespace Havit\Helpers\Forms;


    class Request
    {

        public static function all()
        {
            $retorno = [];
            foreach ($_POST as $key => $POST) {
                if (is_array($POST)) {
                    foreach ($POST as $key2 => $p) {
                        $retorno[$key][$key2] = $p;
                    }
                } else {
                    $retorno[$key] = $POST;
                }
            }

            return $retorno;

        }

        public static function input($parametro)
        {
            if (isset($_POST[$parametro])) {
                if (!empty($_POST[$parametro])) {
                    return $_POST[$parametro];
                } else {
                    return null;
                }
            } else {
                return null;
            }
        }

        public static function set($parametro, $valor)
        {
            $_POST[$parametro] = $valor;
        }

        public static function has($parametro)
        {
            if (isset($_POST[$parametro])) {
                if (!empty($_POST[$parametro])) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }

    }