<?php
/**
 * Created by PhpStorm.
 * User: Gustavo
 */
namespace Havit\Helpers\Forms;


use Havit\Core\Conexao;
use Havit\Core\Utils\StringUtils;

class Grid
{

    static function Gera($vColunas, $aColunasFiltros, $sColunaIndex, $sTabela, $sCondicao = null, $aOrdenacao = null, $sBotaoExtra)
    {
        $aColunas = [];
        $lColunas = [];
        $records = [];
        $records["data"] = [];
        $iDisplayLength = intval($_REQUEST['length']);
        //        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);

        $xx = 0;
        foreach ($vColunas as $v) {
            if (is_array($v)) {
                array_push($aColunas, ["campo" => $v[0][0], "tag" => $v[0][1]]);
                array_push($lColunas, $v[0][0]);
            } else {
                array_push($aColunas, ["campo" => $xx, "tag" => ""]);
                array_push($lColunas, $v);
            }
            $xx++;
        }

        $c = Conexao::getInstance();
        /*
         * Pagina��o
         */
        $sLimite = "";
        if (isset($_GET['start']) && $_GET['length'] != '-1') {
            $sLimite = "LIMIT " . intval($_GET['start']) . ", " . intval($_GET['length']);
        }

        /*
         * Ordena��o
         */
        $sOrdenacao = "";
        if (isset($_GET['iSortCol_0'])) {
            $sOrdenacao = "ORDER BY  ";
            for ($i = 0; $i < intval($_GET['iSortingCols']); $i++) {
                if (is_array($aColunas[intval($_GET['iSortCol_' . $i])])) {

                }
                if ($_GET['bSortable_' . intval($_GET['iSortCol_' . $i])] == "true") {
                    $sOrdenacao .= "`" . $aColunas[intval($_GET['iSortCol_' . $i])]["campo"] . "` " . addslashes($_GET['sSortDir_' . $i]) . ", ";
                }
            }

            $sOrdenacao = substr_replace($sOrdenacao, "", -2);
            if ($sOrdenacao == "ORDER BY") {
                $sOrdenacao = "";
            }
        }

        if (isset($aOrdenacao)) {
            $sOrdenacao = "ORDER BY  ";
            foreach ($aOrdenacao as $o) {
                $sOrdenacao .= "{$o}, ";
            }
            $sOrdenacao = substr_replace($sOrdenacao, "", -2);
            if ($sOrdenacao == "ORDER BY") {
                $sOrdenacao = "";
            }


        }

        /*
         * Filtrando
         */
        $sWhere = "";
        if (isset($sCondicao) && $sCondicao != "") {
            $sWhere = $sCondicao . " ";
        }
        if ((isset($_GET['search']['value']) && $_GET['search']['value'] != "")) {
            {
                if (isset($sCondicao) && $sCondicao != "") {
                    $sWhere .= " AND (";
                } else {
                    $sWhere = "WHERE (";
                }
                for ($i = 0; $i < count($aColunasFiltros); $i++) {
                    $sWhere .= "" . $aColunasFiltros[$i] . " LIKE '%" . addslashes($_GET['search']['value']) . "%' OR ";
                }
                $sWhere = substr_replace($sWhere, "", -3);
                $sWhere .= ')';
            }
        }

        /* Filtrando coluna individualmente */
        for ($i = 0; $i < count($aColunasFiltros); $i++) {
            if (isset($_GET['bSearchable_' . $i]) && $_GET['bSearchable_' . $i] == "true" && $_GET['sSearch_' . $i] != '') {
                if ($sWhere == "") {
                    $sWhere = "WHERE ";
                } else {
                    $sWhere .= " AND ";
                }
                $sWhere .= "`" . $aColunasFiltros[$i] . "` LIKE '%" . addslashes($_GET['sSearch_' . $i]) . "%' ";
            }
        }
        $sQuery = "SELECT $sColunaIndex FROM $sTabela $sCondicao";
        $c->executaConsulta($sQuery);
        $iTotal = $c->qtd;

        if ($sWhere != "") {
            $sQuery = "
            SELECT $sColunaIndex
            FROM   $sTabela
            $sWhere
        ";
            $c->executaConsulta($sQuery);

            $iFilteredTotal = $c->qtd;
        } else {
            $iFilteredTotal = $iTotal;
        }

        $sQuery = "
        SELECT " . str_replace(" , ", " ", implode(", ", $lColunas)) . "
        FROM  $sTabela
        $sWhere
        $sOrdenacao
        $sLimite
    ";
        //   echo $sQuery;exit();
        $c->executaConsulta($sQuery);

        /*
         * Sa�da
         */
        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotal;
        $records["recordsFiltered"] = $iFilteredTotal;

        while ($c->Resultado()) {
            $row = [];
            for ($i = 0; $i < count($aColunas); $i++) {
                $vlLinha = StringUtils::fixToBr($aColunas[$i]["campo"], $c->linha[$i]);
                if ($aColunas[$i]["tag"] != "") {
                    //                    $row[] = str_replace("{0}", mb_convert_encoding($c->linha[$i], "UTF-8"), $aColunas[$i]["tag"]);
                    $linhaAtual = $aColunas[$i]["tag"];
                    if (preg_match_all('/{+(.*?)}/', $aColunas[$i]["tag"], $matches)) {
                        foreach ($matches[1] as $m) {
                            $valor = intval($m);
                            $linhaAtual = str_replace("{" . $m . "}", utf8_encode($c->linha[$aColunas[$valor]["campo"]]), $linhaAtual);
                        }
                    }
                    $row[] = $linhaAtual;
                } else
                    $row[] = utf8_encode($vlLinha);
            }
            $linhaBotoes = "";
            if ($sBotaoExtra != null) {
                foreach ($sBotaoExtra as $botao) {

                    if (preg_match_all('/{+(.*?)}/', $botao, $matches)) {
                        foreach ($matches[1] as $m) {
                            $valor = intval($m);
                            $botao = str_replace("{" . $m . "}", utf8_encode($c->linha[$aColunas[$valor]["campo"]]), $botao);
                        }
                    }
                    $linhaBotoes .= utf8_encode($botao);
                }
            }
            $row[] = $linhaBotoes;
            $records["data"][] = $row;
        }
        header('Content-Type: text/html; charset=utf-8');

        return json_encode($records);
    }

    static function Dropdown($titulo = 'A��es', array $botoes)
    {
        $html = '';
        $html .= '<div class="btn-group"><button class="btn blue dropdown-toggle" type="button" data-toggle="dropdown">' . $titulo . ' <i class="fa fa-angle-down"></i>
										</button><ul class="dropdown-menu" role="menu">';
        foreach ($botoes as $btn) {
            $btn = str_replace('btn default ', '', $btn);
            $html .= "<li>$btn</li>";
        }

        $html .= '</ul></div>';
        return array($html);
    }

    static function Editar($texto = "Editar")
    {
        return "<a href='../Editar/?id={0}' class='btn default purple'><i class='fa fa-edit'> " . $texto . "</i></a>";
    }

    static function Visualizar($texto = "Visualizar")
    {
        return "<a href='../Visualizar/?id={0}' class='btn default blue'><i class='fa fa-search'> " . $texto . "</i></a>";
    }

    static function Remover($texto = "Remover")
    {
        return "<button onclick=\"javascript:havit.swal.remover('../Deletar/?did={0}')\" class='btn default red'><i class='fa fa-trash-o'> " . $texto . "</i></button>";
    }

    /**
     * @param $link
     * @param $texto
     * @param $cor
     * @param $icone
     * @param $target
     *
     * @return string
     */
    static function Botao($link, $texto, $cor, $icone, $target = "_self")
    {
        return "<a href='" . $link . "' class='btn default " . $cor . "' target='" . $target . "'><i class='fa fa-" . $icone . "'> " . $texto . "</i></a>";
    }


}