<?php
    namespace Havit\Helpers;
    /**
     * Created by PhpStorm.
     * User: Gustavo
     * Date: 22/11/2014
     * Time: 16:03
     */
    class Swal
    {

        public static function mensagem($titulo, $msg)
        {
            $GLOBALS['gui_js'] .= 'swal({title: "' . $titulo . '", text: "' . $msg . '",  timer:4000});';
        }

        public static function sucesso($msg, $timer = true)
        {

            if ($timer)
                $GLOBALS['gui_js'] .= 'swal({title: "Sucesso!", text: "' . $msg . '", type: "success",timer:4000});';
            else
                $GLOBALS['gui_js'] .= 'swal({title: "Sucesso!", text: "' . $msg . '", type: "success"});';
        }

        public static function erro($msg, $timer = true)
        {
            if ($timer)
                $GLOBALS['gui_js'] .= 'swal({title: "Erro!", text: "' . $msg . '", type: "error", timer:4000});';
            else
                $GLOBALS['gui_js'] .= 'swal({title: "Erro!", text: "' . $msg . '", type: "error"});';
        }

        public static function customHtmlMsg($titulo, $msg, $type)
        {
            $GLOBALS['gui_js'] .= 'swal({title: "' . $titulo . '", text: "' . $msg . '",  type:"' . $type . '", html: true});';

        }

        public static function mensagemTipo($tipo, $msg, $timer = true)
        {
            if ($tipo == "sucesso") {
                self::sucesso($msg, $timer);
            } else if ($tipo == "erro") {
                self::erro($msg, $timer);
            }

        }
    }