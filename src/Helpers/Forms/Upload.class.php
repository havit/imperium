<?php
/**
 * User: iurigustavo
 */

namespace Havit\Helpers\Forms;


use Havit\Core\Utils\Slug;
use Havit\Helpers\Session;
use Havit\Helpers\Swal;

class Upload
{


    /**
     * @param      $arquivo
     * @param      $pasta
     * @param bool $gerarThumbnail
     * @param null $subPasta
     * @param null $w
     * @param null $h
     *
     * @return null|string
     */
    public static function Upload($arquivo, $pasta, $gerarThumbnail = false, $subPasta = null, $w = null, $h = null)
    {
        if ($arquivo['name'] == "")
            $arquivo = "";

        $error = false;
        // Confere se existe imagem declarada na variavel
        if (!empty($arquivo)) {
            // Instanciamos o objeto Upload
            $handle = new \Upload($arquivo);
            // Ent?o verificamos se o arquivo foi carregado corretamente
            if ($handle->uploaded) {

                $handle->file_new_name_body = Slug::makeSlug($handle->file_src_name_body);

                $handle->file_max_size = 20971520;

                //if ($pasta != 'banners')  {
                // Definimos md5 para o arquivo
                //$md5filename				= md5(uniqid());
                //$handle->file_new_name_body = $md5filename;
                //}
                // Definimos as configurações desejadas da imagem maior
                $handle->dir_chmod = 0777;

                // Qualidade
                $handle->jpeg_quality = 80;


                // REDIMENSIONA OU NAO A IMAGEM
                if ($w != null and $h != null) {
                    $handle->image_resize = true;
                    $handle->image_x = $w;
                    $handle->image_y = $h;
                } else {
                    $handle->image_resize = false;
                    $handle->image_ratio_y = false;
                    $handle->image_ratio_x = false;
                }

                $handle->mime_check = true;
                $handle->file_safe_name = true;

                $pasta_destino = "../public/upload/" . $pasta . "/";
                if ($subPasta != null) {
                    $pasta_destino = "../public/upload/" . $pasta . "/" . $subPasta . "/";
                }

                $handle->Process($pasta_destino);
                // Em caso de sucesso no upload podemos fazer outras a??es como insert em um banco de cados
                if ($handle->processed) {
                    // Exibimos a informa??o de sucesso ou qualquer outra a??o de sua escolha
                } else {
                    // Em caso de erro listamos o erro abaixo
                    $error .= $handle->error;
                }

                if (!$error) {
                    // GERA THUMBNAIL
                    if ($gerarThumbnail) {

                        $handle->file_new_name_body = Slug::makeSlug($handle->file_src_name_body);
                        // Aqui nos definimos nossas configura??es de imagem do thumbs

                        if (empty($w))
                            $w = 100;
                        if (empty($h))
                            $h = 100;

                        //$handle->file_new_name_body = $md5filename;
                        $handle->dir_chmod = 0777;

                        $handle->image_resize = true;
                        $handle->image_x = $w;
                        $handle->image_y = $h;

                        $pasta_destinoTb = "../public/upload/" . $pasta . "/thumbs/";
                        if (is_numeric($pasta)) {
                            $pasta_destinoTb = "../public/upload/" . $pasta . "/" . $subPasta . "/thumbs/";
                        }
                        $handle->Process($pasta_destinoTb);


                    }
                }

                // Excluimos os arquivos temporarios
                $handle->Clean();
            } else {
                // Em caso de erro listamos o erro abaixo
                $error .= $handle->error;
            }
            // Aqui somente recupero o nome da imagem caso queira fazer um insert em banco de dados
            $nome_da_imagem = $handle->file_dst_name;


            if ($error) {
                return null;
            } else {
                return $nome_da_imagem;
            }
        } else {
            return null;
        }


    }

    public static function UploadNoticias($arquivo, $pasta)
    {
        if ($arquivo['name'] == "")
            $arquivo = "";

        $error = false;
        // Confere se existe imagem declarada na variavel
        if (!empty($arquivo)) {
            // Instanciamos o objeto Upload
            $handle = new \Upload($arquivo);
            // Ent?o verificamos se o arquivo foi carregado corretamente
            if ($handle->uploaded) {

                $handle->file_new_name_body = Slug::makeSlug($handle->file_src_name_body);

                $handle->file_max_size = 20971520;

                //if ($pasta != 'banners')  {
                // Definimos md5 para o arquivo
                //$md5filename				= md5(uniqid());
                //$handle->file_new_name_body = $md5filename;
                //}
                // Definimos as configurações desejadas da imagem maior
                $handle->dir_chmod = 0777;

                // Qualidade
                $handle->jpeg_quality = 80;


                $handle->image_resize = true;
                $handle->image_x = 800;
                $handle->image_y = 600;

                $handle->mime_check = true;
                $handle->file_safe_name = true;

                $pasta_destino = "../public/upload/" . $pasta . "/";

                $handle->Process($pasta_destino);
                // Em caso de sucesso no upload podemos fazer outras a??es como insert em um banco de cados
                if ($handle->processed) {
                    // Exibimos a informa??o de sucesso ou qualquer outra a??o de sua escolha
                } else {
                    // Em caso de erro listamos o erro abaixo
                    $error .= $handle->error;
                }

                if (!$error) {
                    // GERA THUMBNAIL
                    // Aqui nos definimos nossas configura??es de imagem do thumbs

                    $handle->file_new_name_body = Slug::makeSlug($handle->file_src_name_body);
                    //$handle->file_new_name_body = $md5filename;
                    $handle->dir_chmod = 0777;

                    $handle->image_resize = true;
                    $handle->image_x = 300;
                    $handle->image_y = 225;

                    $pasta_destinoTb = "../public/upload/" . $pasta . "/post/";
                    $handle->Process($pasta_destinoTb);

                    $handle->file_new_name_body = Slug::makeSlug($handle->file_src_name_body);

                    //$handle->file_new_name_body = $md5filename;
                    $handle->dir_chmod = 0777;

                    $handle->image_resize = true;
                    $handle->image_x = 100;
                    $handle->image_y = 100;

                    $pasta_destinoTb = "../public/upload/" . $pasta . "/thumbs/";
                    $handle->Process($pasta_destinoTb);

                }

                // Excluimos os arquivos temporarios
                $handle->Clean();
            } else {
                // Em caso de erro listamos o erro abaixo
                $error .= $handle->error;
            }
            // Aqui somente recupero o nome da imagem caso queira fazer um insert em banco de dados
            $nome_da_imagem = $handle->file_dst_name;


            if ($error) {
                return null;
            } else {
                return $nome_da_imagem;
            }
        } else {
            return null;
        }


    }

    public static function UploadGaleria($arquivo, $pasta)
    {
        if ($arquivo['name'] == "")
            $arquivo = "";

        $error = false;
        // Confere se existe imagem declarada na variavel
        if (!empty($arquivo)) {
            // Instanciamos o objeto Upload
            $handle = new \Upload($arquivo);
            // Ent?o verificamos se o arquivo foi carregado corretamente
            if ($handle->uploaded) {

                $handle->file_new_name_body = Slug::makeSlug($handle->file_src_name_body);

                $handle->file_max_size = 20971520;

                //if ($pasta != 'banners')  {
                // Definimos md5 para o arquivo
                //$md5filename				= md5(uniqid());
                //$handle->file_new_name_body = $md5filename;
                //}
                // Definimos as configurações desejadas da imagem maior
                $handle->dir_chmod = 0777;

                // Qualidade
                $handle->jpeg_quality = 80;


                $handle->image_resize = true;
                $handle->image_x = 800;
                $handle->image_y = 600;

                $handle->mime_check = true;
                $handle->file_safe_name = true;

                $pasta_destino = "../public/upload/Galerias/" . $pasta . "/";

                $handle->Process($pasta_destino);
                // Em caso de sucesso no upload podemos fazer outras a??es como insert em um banco de cados
                if ($handle->processed) {
                    // Exibimos a informa??o de sucesso ou qualquer outra a??o de sua escolha
                } else {
                    // Em caso de erro listamos o erro abaixo
                    $error .= $handle->error;
                }

                if (!$error) {
                    // GERA THUMBNAIL
                    // Aqui nos definimos nossas configura??es de imagem do thumbs

                    $handle->file_new_name_body = Slug::makeSlug($handle->file_src_name_body);

                    //$handle->file_new_name_body = $md5filename;
                    $handle->dir_chmod = 0777;

                    $handle->image_resize = true;
                    $handle->image_x = 225;
                    $handle->image_y = 168;

                    $pasta_destinoTb = "../public/upload/Galerias/" . $pasta . "/thumbs/";
                    $handle->Process($pasta_destinoTb);

                }

                // Excluimos os arquivos temporarios
                $handle->Clean();
            } else {
                // Em caso de erro listamos o erro abaixo
                $error .= $handle->error;
            }
            // Aqui somente recupero o nome da imagem caso queira fazer um insert em banco de dados
            $nome_da_imagem = $handle->file_dst_name;


            if ($error) {
                return null;
            } else {
                return $nome_da_imagem;
            }
        } else {
            return null;
        }


    }


}