<?php

/**
 * Created by PhpStorm.
 * User: Gustavo
 * Date: 24/11/2014
 * Time: 20:57
 */
namespace Havit\Core\Helpers\SQL;

use Exception;
use Havit\Core\Conexao;
use Havit\Core\Utils\StringUtils;

class SQL
{
    public $sql = "";
    private $whereRaw;
    private $parametrostemp = [];
    private $tabela;
    private $where = [];
    private $campos = [];
    private $top = null;
    private $distinct = false;
    private $ordem = [];
    private $group = [];
    private $join = [];
    private $_take = null;
    /** @var $conexao Conexao */
    private $conexao;
    private $_skip;

    function __construct()
    {
        $this->conexao = Conexao::getInstance();
    }


    function whereRaw($where = null, $parametro = null)
    {
        if (!empty($where)) {
            $this->whereRaw = $where;
        }
        $this->parametrostemp = $parametro;

        return $this;
    }

    public function table($tbl)
    {
        $this->tabela = $tbl;

        return $this;

    }

    public function distinct()
    {
        $this->distinct = true;

        return $this;
    }

    function select($campos)
    {
        if (is_array($campos)) {
            foreach ($campos as $campo) {
                array_push($this->campos, ["campo" => $campo, "funcao" => null]);
            }
        } else {
            array_push($this->campos, ["campo" => $campos, "funcao" => null]);
        }

        return $this;
    }

    function set($campo, $valor, $funcao = null)
    {

        array_push($this->campos, ["campo" => $campo, "valor" => $valor, "funcao" => $funcao]);


        return $this;
    }

    function delete()
    {
        $this->montaSql("DELETE");

        $return = $this->conexao->executaConsulta($this->sql, $this->parametrostemp);
        $this->limpaBuffer();
        return $return;
    }

    private function montaSql($tipo)
    {
        if ($tipo == "SELECT") {

            $this->montaSelect();
            $this->montaFrom();
            $this->montaWhere();
            $this->montaGroupBy();
            $this->montaOrderBy();
            $this->montaLimit();

        } else if ($tipo == "DELETE") {
            $this->montaDelete();
            $this->montaWhere();
        }
        if ($tipo == "INSERT") {
            $this->montaInsert();
        }

        //            echo $this->sql;
    }

    private function montaSelect()
    {
        $campo = [];
        $this->sql = "SELECT ";
        if ($this->distinct) {
            $this->sql .= " DISTINCT ";
        }
        $qtdCampos = sizeof($this->campos);
        if ($qtdCampos == 0) {
            $this->sql .= $this->tabela . ".*";
        } else {
            foreach ($this->campos as $c) {
                if ($c['funcao'] != null) {
                    $campo[] = $c['funcao'] . "(" . $c['campo'] . ") as " . $c['campo'] . " ";
                } else {
                    $campo[] = $c['campo'];
                }
            }
            $this->sql .= implode(",", $campo);
        }
    }

    private function montaFrom()
    {
        $this->sql .= " FROM " . $this->tabela;

        foreach ($this->join as $join) {
            $this->sql .= " " . $join['tipo'] . " " . $join['tabela'] . " ON " . $join['condicao'];
        }
    }

    private function montaWhere()
    {
        $primeiro = true;
        if (isset($this->whereRaw)) {
            $this->sql .= " WHERE " . $this->whereRaw;
        } else {
            foreach ($this->where as $w) {
                if ($primeiro) {
                    $primeiro = false;
                    $this->sql .= " WHERE ";
                } else {
                    $this->sql .= " " . $w['conjuncao'] . " ";
                }
                if (strstr($w['operador'], "in")) {

                    if (is_array($w['valor'])) {
                        $lValor = [];
                        foreach ($w['valor'] as $valor) {
                            array_push($lValor, "?");
                            array_push($this->parametrostemp, $valor);
                        }
                        $this->sql .= " (" . $w['campo'] . " " . $w['operador'] . " (" . implode(",", $lValor) . "))";
                    } else {
                        $this->sql .= " (" . $w['campo'] . " " . $w['operador'] . " (?)) ";
                        array_push($this->parametrostemp, $w['valor']);
                    }
                } else if (strstr($w['operador'], "between")) {

                    $lValor = [];
                    foreach ($w['valor'] as $valor) {
                        array_push($lValor, "?");
                        array_push($this->parametrostemp, $valor);
                    }
                    $this->sql .= " (" . $w['campo'] . " " . $w['operador'] . " " . implode(" AND ", $lValor) . ")";

                } else {

                    $this->sql .= " (" . $w['campo'] . " " . $w['operador'] . " ? )";
                    array_push($this->parametrostemp, $w['valor']);
                }
            }
        }
    }

    private function montaGroupBy()
    {
        $qtdColunas = sizeof($this->group);
        if ($qtdColunas > 0) {
            $this->sql .= " GROUP BY " . implode(",", $this->group) . " ";
        }


    }

    private function montaOrderBy()
    {
        $qtdColunas = sizeof($this->ordem);
        if ($qtdColunas > 0) {
            $this->sql .= " ORDER BY " . implode(",", $this->ordem) . " ";
        }

    }

    private function montaLimit()
    {
        if (isset($this->_take)) {
            $this->sql .= " LIMIT " . $this->_take;
        }

        if (isset($this->_skip)) {
            $this->sql .= " OFFSET " . $this->_skip;
        }
    }

    private function montaDelete()
    {
        $this->sql = "DELETE FROM ";
        $this->sql .= $this->tabela;
    }

    private function montaInsert()
    {
        $campo = [];
        $valores = [];
        $this->sql = "INSERT INTO " . $this->tabela . " ";

        foreach ($this->campos as $c) {
            $campo[] = $c['campo'];
        }
        $this->sql .= "(" . implode(",", $campo) . ") VALUES ";

        foreach ($this->campos as $c) {
            if ($c['funcao'] != null) {
                if ($c['valor'] == null) {
                    $valores[] = $c['funcao'];
                } else {
                    $valores[] = $c['funcao'];
                    array_push($this->parametrostemp, $c['valor']);
                }
            } else {
                $valores[] = "?";
                array_push($this->parametrostemp, $c['valor']);
            }
        }
        $this->sql .= "(" . implode(",", $valores) . ") ";
    }

    public function joinLeft($tabela, $condicao, $colunas = null)
    {
        return self::_join("LEFT JOIN", $tabela, $condicao, $colunas);
    }

    public function joinRight($tabela, $condicao, $colunas = null)
    {
        return self::_join("RIGHT JOIN", $tabela, $condicao, $colunas);
    }

    private function _join($tipo, $tabela, $condicao, $colunas = null)
    {
        array_push($this->join, ["tipo" => $tipo, "tabela" => $tabela, "condicao" => $condicao, "colunas" => $colunas]);

        if ($colunas != null) {
            foreach ($colunas as $col) {
                $this->select($tabela . "." . $col);
            }
        }

        return $this;
    }

    function count($campo)
    {
        array_push($this->campos, ["campo" => $campo, "funcao" => "COUNT"]);

        return $this;

    }

    function max($campo)
    {
        array_push($this->campos, ["campo" => $campo, "funcao" => "MAX"]);

    }

    function sum($campo)
    {
        array_push($this->campos, ["campo" => $campo, "funcao" => "SUM"]);

        return $this;

    }

    function whereEq($campo, $valor, $conjuncao = 'and')
    {

        return self::where($campo, "=", $valor, $conjuncao);
    }

    function where($campo, $operador, $valor, $conjuncao = 'and')
    {
        array_push($this->where, ["campo" => $campo, "operador" => $operador, "conjuncao" => $conjuncao, "valor" => $valor]);

        return $this;
    }

    function whereIn($campo, $valor, $conjuncao = 'and')
    {
        array_push($this->where, ["campo" => $campo, "operador" => "in", "conjuncao" => $conjuncao, "valor" => $valor]);

        return $this;
    }

    function whereBetween($campo, $valores, $conjuncao = 'and')
    {
        array_push($this->where, ["campo" => $campo, "operador" => "between", "conjuncao" => $conjuncao, "valor" => $valores]);

        return $this;

    }

    function orWhere($campo, $operador, $valor)
    {

        $this->where($campo, $operador, $valor, 'or');

        return $this;
    }

    function orderBy($coluna, $direcao = 'asc')
    {
        $direcao = strtolower($direcao) == 'asc' ? 'asc' : 'desc';

        $this->ordem[] = $coluna . " " . $direcao;

        return $this;
    }

    function groupBy($coluna)
    {
        $this->group[] = $coluna;

        return $this;
    }

    function take($qtd)
    {
        $this->_take = $qtd;

        return $this;
    }

    function skip($qtd)
    {
        $this->_skip = $qtd;

        return $this;
    }

    public function first()
    {
        if (empty($this->sql)) {
            $this->montaSql("SELECT");
        }
        $this->conexao->executaConsulta($this->sql, $this->parametrostemp);
        $class = $this->tabela;
        $newClass = "App\\Dao\\" . $class;
        $obj = new  $newClass;
        try {
            $this->conexao->res->setFetchMode(\PDO::FETCH_ASSOC);
            if ($this->conexao->Resultado()) {
                foreach (array_keys($this->conexao->linha) as $coluna) {
                    call_user_func([$obj, StringUtils::camelCase("set" . ucfirst($coluna))], $this->conexao->linha[$coluna]);
                }
            } else {
                $this->limpaBuffer();

                return false;
            }

            $this->limpaBuffer();

            return $obj;
        } catch (Exception $e) {
            error_log("Falha: " . $e->getMessage() . " URL: " . $GLOBALS['url'], 0);
        }
    }

    private function limpaBuffer()
    {
        $this->sql = "";
        $this->whereRaw = null;
        $this->parametrostemp = [];
        $this->tabela = null;
        $this->where = [];
        $this->campos = [];
        $this->_take = null;
        $this->distinct = false;
        $this->ordem = [];
        $this->group = [];
        $this->join = [];
    }

    public function get()
    {
        if (empty($this->sql)) {
            $this->montaSql("SELECT");
        }
        $this->conexao->executaConsulta($this->sql, $this->parametrostemp);
        $class = $this->tabela;
        try {
            $listaResultados = [];
            $this->conexao->res->setFetchMode(\PDO::FETCH_ASSOC);
            while ($this->conexao->Resultado()) {
                $newClass = "App\\Dao\\" . $class;
                $obj = new  $newClass;
                foreach (array_keys($this->conexao->linha) as $coluna) {
                    call_user_func([$obj, StringUtils::camelCase("set" . ucfirst($coluna))], $this->conexao->linha[$coluna]);
                }
                $listaResultados[] = $obj;
            }

            $this->limpaBuffer();

            return $listaResultados;
        } catch (Exception $e) {
            error_log("Falha: " . $e->getMessage() . " URL: " . $GLOBALS['url'], 0);
        }
    }

    /**
     * @return object|bool
     */
    public function find()
    {
        if (empty($this->sql)) {
            $this->montaSql("SELECT");
        }
        $this->conexao->executaConsulta($this->sql, $this->parametrostemp);
        $this->conexao->res->setFetchMode(\PDO::FETCH_ASSOC);
        $newClass = "App\\DAO\\" . $this->tabela;
        $obj = new $newClass;

        if ($this->conexao->Resultado()) {
            foreach (array_keys($this->conexao->linha) as $coluna) {
                call_user_func([$obj, StringUtils::camelCase("set" . ucfirst($coluna))], $this->conexao->linha[$coluna]);
                // call_user_func_array([$obj, "__set"], [$coluna, $this->conexao->linha[$coluna]]);
            }
        } else {
            $this->limpaBuffer();

            return false;
        }
        $this->limpaBuffer();

        return $obj;
    }

    public function lists()
    {
        $args = func_get_args();
        foreach ($args as $coluna) {
            array_push($this->campos, ["campo" => $coluna, "funcao" => null]);
        }
        if (empty($this->sql)) {
            $this->montaSql("SELECT");
        }
        $this->conexao->executaConsulta($this->sql, $this->parametrostemp);
        try {
            $listaResultados = [];
            $this->conexao->res->setFetchMode(\PDO::FETCH_ASSOC);
            while ($this->conexao->Resultado()) {
                if (func_num_args() > 1) {
                    $obj = [$this->conexao->linha[func_get_arg(0)] => $this->conexao->linha[func_get_arg(1)]];
                } else {
                    $obj = $this->conexao->linha[func_get_arg(0)];
                }
                $listaResultados[] = $obj;
            }

            $this->limpaBuffer();

            return $listaResultados;
        } catch (Exception $e) {
            error_log("Falha: " . $e->getMessage() . " URL: " . $GLOBALS['url'], 0);
        }
    }

    /**
     * @return bool
     */
    public function exists()
    {
        array_push($this->campos, ["campo" => 1, "funcao" => null]);

        if (empty($this->sql)) {
            $this->montaSql("SELECT");
        }
        $this->conexao->executaConsulta($this->sql, $this->parametrostemp);
        $this->conexao->res->setFetchMode(\PDO::FETCH_ASSOC);

        if ($this->conexao->Resultado()) {
            $this->limpaBuffer();

            return true;
        } else {
            $this->limpaBuffer();

            return false;
        }


    }

    public function update(array $array)
    {
        $this->montaUpdate($array);
        $this->montaWhere();

        return $this->conexao->executaConsulta($this->sql, $this->parametrostemp);

    }

    private function montaUpdate(array $campos)
    {
        foreach ($campos as $key => $campo) {
            $campos[] = $key . "=?";
            $valores[] = $campo;
        }


        $this->sql = "UPDATE " . $this->tabela . " SET " . implode(",", $campos);

        if (isset($valores)) {
            foreach ($valores as $vl) {
                array_push($this->parametrostemp, $vl);
            }
        }
    }

    /**
     *
     * @return array
     */
    public function getArray()
    {
        if (empty($this->sql)) {
            $this->montaSql("SELECT");
        }
        $this->conexao->executaConsulta($this->sql, $this->parametrostemp);
        try {
            $listaResultados = [];
            $this->conexao->res->setFetchMode(\PDO::FETCH_ASSOC);
            while ($this->conexao->Resultado()) {
                $obj = [];
                foreach (array_keys($this->conexao->linha) as $coluna) {
                    $obj += [$coluna => $this->conexao->linha[$coluna]];
                }
                $listaResultados[] = $obj;
            }
            $this->limpaBuffer();

            return $listaResultados;
        } catch (Exception $e) {
            error_log("Falha: " . $e->getMessage() . " URL: " . $GLOBALS['url'], 0);
        }
    }

    public function getArrayKeys($key, $descricao)
    {
        $this->select($key);
        $this->select($descricao);
        $this->montaSql("SELECT");
        $this->conexao->executaConsulta($this->sql, $this->parametrostemp);

        try {
            $listaResultados = [];
            $this->conexao->res->setFetchMode(\PDO::FETCH_BOTH);
            while ($this->conexao->Resultado()) {
                $listaResultados[$this->conexao->linha[0]] = $this->conexao->linha[1];
            }
            $this->limpaBuffer();

            return $listaResultados;
        } catch (Exception $e) {
            error_log("Falha: " . $e->getMessage() . " URL: " . $GLOBALS['url'], 0);
        }
    }

    public function getJson($class = null)
    {
        $this->montaSql("SELECT");

        $this->conexao->executaConsulta($this->sql, $this->parametrostemp);
        $newClass = "App\\DAO\\" . $this->tabela;
        try {
            return json_encode($this->conexao->res->fetchAll(\PDO::FETCH_CLASS, $newClass));
        } catch (Exception $e) {
            error_log("Falha: " . $e->getMessage() . " URL: " . $GLOBALS['url'], 0);
        }

    }

    /**
     * @param $class
     *
     * @return mixed
     */
    public function getClass($class)
    {
        $this->montaSql("SELECT");
        $this->conexao->executaConsulta($this->sql, $this->parametrostemp);
        if ($class == null) {
            $class = $this->tabela . "VO";
        }
        try {
            $this->conexao->res->setFetchMode(\PDO::FETCH_CLASS, $class);

            return $this->conexao->res->fetch();
        } catch (Exception $e) {
            error_log("Falha: " . $e->getMessage() . " URL: " . $GLOBALS['url'], 0);
        }

    }

    public function getObj(&$obj = null)
    {
        $this->montaSql("SELECT");
        $this->conexao->executaConsulta($this->sql, $this->parametrostemp);
        $this->conexao->res->setFetchMode(\PDO::FETCH_ASSOC);

        if ($this->conexao->Resultado()) {
            foreach (array_keys($this->conexao->linha) as $coluna) {
                call_user_func([$obj, StringUtils::camelCase("set" . ucfirst($coluna))], $this->conexao->linha[$coluna]);
                // call_user_func_array([$obj, "__set"], [$coluna, $this->conexao->linha[$coluna]]);
            }

            return true;
        }

        return false;
    }

    public function getResultado()
    {
        $this->montaSql("SELECT");
        $this->conexao->executaConsulta($this->sql, $this->parametrostemp);
        $this->conexao->res->setFetchMode(\PDO::FETCH_ASSOC);
        if ($this->conexao->Resultado()) {
            return true;
        }

        return false;
    }

    /**
     * @param $coluna
     *
     * @return mixed
     */
    public function value($coluna)
    {
        if (empty($this->sql)) {
            $this->montaSql("SELECT");
        }
        $this->conexao->executaConsulta($this->sql, $this->parametrostemp);
        try {
            $this->conexao->res->setFetchMode(\PDO::FETCH_ASSOC);
            if ($this->conexao->Resultado()) {
                $this->limpaBuffer();

                return $this->conexao->linha[$coluna];
            } else {

                $this->limpaBuffer();

                return false;
            }


        } catch (Exception $e) {
            error_log("Falha: " . $e->getMessage() . " URL: " . $GLOBALS['url'], 0);
        }

    }

    public function query($sql, $parametros = null)
    {
        $this->conexao->executaConsulta($sql, $parametros);
        try {
            $this->conexao->res->setFetchMode(\PDO::FETCH_ASSOC);
            return $this->conexao->res->fetchAll();

        } catch (\Exception $e) {
            error_log("Falha: " . $e->getMessage() . " URL: " . $GLOBALS['url'], 0);
        }
    }

}