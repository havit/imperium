<?php
    /**
     * User: Gustavo
     * Date: 02/11/13
     * Time: 01:01
     */
    namespace Havit\Core\Template;

    class Layout
    {
        function RenderBody()
        {
            return $GLOBALS['_conteudo'];
        }

        function getTitulo()
        {
            if (isset($GLOBALS['_titulo'])) {
                return $GLOBALS['_titulo'];
            } else {
                return "";
            }

        }

        function getDescricao()
        {
            if (isset($GLOBALS['_descricao'])) {
                return $GLOBALS['_descricao'];
            } else {
                return "";
            }
        }

        public static function createSection($section)
        {
            ob_start();
        }

        public static function endSection($section)
        {
            $GLOBALS['section_' . $section] = ob_get_clean();
        }

        public static function getSection($section)
        {

            if (isset($GLOBALS['section_' . $section])) {
                echo $GLOBALS['section_' . $section];
            } else {
                return "";
            }


        }

    }