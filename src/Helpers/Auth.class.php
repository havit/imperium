<?php
/**
 * User: iurigustavo
 */

namespace Havit\Helpers;


use App\DAO\Usuarios;
use Havit\Helpers\Session;

class Auth
{
    public static function autenticar()
    {

    }

    public static function autenticarPorId($id)
    {
        $usuario = Usuarios::find($id);
        if ($usuario) {
            Session::set('usuario', $usuario);
        }


    }

    public static function atualizar()
    {
        $usuario = Usuarios::find(self::usuario()->getId());
        if ($usuario) {
            Session::set('usuario', $usuario);
        }
    }


    /**
     * @return \App\DAO\Usuarios
     */
    public static function usuario()
    {
        return Session::get('usuario');
    }

    /**
     * @return bool
     */
    public static function isAutenticado()
    {
        if (Session::exists('usuario')) {
            return true;
        } else
            return false;

    }

    public static function desconectar()
    {
        Session::forget('usuario');
    }

    public static function conectar($usuario)
    {
        Session::set('usuario', $usuario);
    }
}