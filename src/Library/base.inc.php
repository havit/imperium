<?php
/**
 * User: Gustavo
 * Date: 14/01/13
 * Time: 20:54
 */

error_reporting(E_ALL);
ini_set('display_errors', 'On');
date_default_timezone_set("Brazil/West");

$GLOBALS['app'] = include PROJECT_ROOT . DS . 'config' . DS . 'app.php';
$GLOBALS['config'] = include PROJECT_ROOT . DS . 'config' . DS . 'config.php';
$GLOBALS['conteudo'] = include PROJECT_ROOT . DS . 'config' . DS . 'conteudo.php';
$GLOBALS['database'] = include PROJECT_ROOT . DS . 'config/database.php';
$GLOBALS['email'] = include PROJECT_ROOT . DS . 'config/email.php';
$GLOBALS['gui_js'] = "";

DEFINE('BASE_PATH', ($GLOBALS['config']['ambientes'][$GLOBALS['app']['ambiente']]['caminho_base']));

require_once(PROJECT_ROOT . DS . 'config' . DS . 'database.php');

// Importa Helpers
foreach (glob(ENGINE . DS . "Helpers" . DS . "*.php") as $filename) {
    include $filename;
}
// Importa Sql
foreach (glob(ENGINE . DS . "Helpers" . DS . "SQL" . DS . "*.php") as $filename) {
    include $filename;
}
// Importa Utils
foreach (glob(ENGINE . DS . "Helpers" . DS . "Utils" . DS . "*.php") as $filename) {
    include $filename;
}
// Importa Template
foreach (glob(ENGINE . DS . "Helpers" . DS . "Template" . DS . "*.php") as $filename) {
    include $filename;
}
// Importa Template
foreach (glob(ENGINE . DS . "Helpers" . DS . "Forms" . DS . "*.php") as $filename) {
    include $filename;
}
// Importa Template
foreach (glob(ENGINE . DS . "Helpers" . DS . "Forms" . DS . "Builder" . DS . "*.php") as $filename) {
    include $filename;
}
foreach (glob(ENGINE . DS . "Core" . DS . "*.php") as $filename) {
    include $filename;
}
//    require_once(ENGINE . DS . 'core' . DS . 'Template.php');
require_once(ENGINE . DS . 'Connection' . DS . 'Conexao.class.php');
require_once(ENGINE . DS . 'Library' . DS . 'variaveis.inc.php');
//    require_once(ENGINE . DS . 'core' . DS . 'Controller.php');
//    require_once(ENGINE . DS . 'core' . DS . 'Model.php');
//require_once(VENDOR . DS . 'mail' . DS . 'class.phpmailer.php');
require_once(ENGINE . DS . 'Vendor' . DS . 'upload' . DS . 'upload.php');
require_once(ENGINE . DS . 'Vendor' . DS . 'PFBC' . DS . 'Form.php');

// Ativa Segurança
\Havit\Core\Seguranca::QueryProtect();

// Componente de Interface
$GUI = new \Havit\Helpers\GUI();
Havit\Helpers\Session::init();
foreach ($GLOBALS['database'][$GLOBALS['app']['ambiente']] as $key => $_dt) {
    // Importa datasource
    foreach (glob(PROJECT_ROOT . DS . 'app' . DS . "database" . DS . $key . DS . "entity" . DS . "*.php") as $filename) {
        include $filename;
    }
    // Importa datasource
    foreach (glob(PROJECT_ROOT . DS . 'app' . DS . "database" . DS . $key . DS . "model/*.php") as $filename) {
        include $filename;
    }
    // Importa datasource
    foreach (glob(PROJECT_ROOT . DS . 'app' . DS . "database" . DS . $key . DS . "dao/*.php") as $filename) {
        include $filename;
    }
    // Importa datasource
    foreach (glob(PROJECT_ROOT . DS . 'app' . DS . "database" . DS . $key . DS . "logic/*.php") as $filename) {
        include $filename;
    }
}
foreach (glob(PROJECT_ROOT . DS . "app" . DS . "Forms" . DS . "*.php") as $filename) {
    include $filename;
}