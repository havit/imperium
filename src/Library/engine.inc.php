<?php
/**
 * User: Iuri Gustavo
 * Date: 27/07/13
 * Time: 08:45
 */

namespace Havit\Library;

class Engine
{

    // session_save_path(dirname($_SERVER['DOCUMENT_ROOT']).'/public_html/tmp');


    /** Checa se o ambiente � de desenvolvimento e mostra os erros **/
    static function setRelatoriosErrors()
    {
        global $app;
        if ($GLOBALS['app']['ambiente'] == 'desenvolvimento') {
            error_reporting(E_ALL);
            ini_set('display_errors', 'On');
        } else {
            error_reporting(E_ALL);
            ini_set('display_errors', 'Off');
            //  ini_set('log_errors', 'On');
            //  ini_set('error_log', ROOT . DS . 'tmp' . DS . 'logs' . DS . 'error.log');
        }
    }

    static function stripSlashesDeep($value)
    {
        $value = is_array($value) ? array_map('stripSlashesDeep', $value) : stripslashes($value);

        return $value;
    }

    static function removeMagicQuotes()
    {
        if (get_magic_quotes_gpc()) {
            $_GET = stripSlashesDeep($_GET);
            $_POST = stripSlashesDeep($_POST);
            $_COOKIE = stripSlashesDeep($_COOKIE);
        }
    }

    static function unregisterGlobals()
    {
        if (ini_get('register_globals')) {
            $array = ['_SESSION', '_POST', '_GET', '_COOKIE', '_REQUEST', '_SERVER', '_ENV', '_FILES'];
            foreach ($array as $value) {
                foreach ($GLOBALS[$value] as $key => $var) {
                    if ($var === $GLOBALS[$key]) {
                        unset($GLOBALS[$key]);
                    }
                }
            }
        }
    }


    /** ONDE A M�GICA ACONTECE :D **/

    static function callHook()
    {

        global $url;
        //        global $default;
        global $config;

        if (!isset($url)) {
            if ($config['padroes']['area'] == $config['areas'][0]) {
                header("location: " . $config['padroes']['controller'] . "/" . $config['padroes']['acao'] . "/");
                echo("<script>location.href= " . $config['padroes']['controller'] . "/" . $config['padroes']['acao'] . "/</script>");
            } else {
                header("location: " . $config['padroes']['area'] . "/" . $config['padroes']['controller'] . "/" . $config['padroes']['acao'] . "/");
                echo("<script>location.href= " . $config['padroes']['area'] . "/" . $config['padroes']['controller'] . "/" . $config['padroes']['acao'] . "/</script>");
            }
            $area = $config['padroes']['area'];
            $controller = $config['padroes']['controller'];
            $acao = $config['padroes']['acao'];
            exit;
        } else {
            $urlArray = explode("/", $url);
            $area = $urlArray[0];
            if (!empty($urlArray)) {
                if (isset($urlArray[1])) {
                    $controller = $urlArray[1];
                }
            }
            array_shift($urlArray);
            if (isset($urlArray[2])) {
                $acao = $urlArray[2];
                array_shift($urlArray);
            } else {
                $acao = 'Index'; // Default Action
            }
        }
        if ($acao == null) {
            $acao = "Index";
        }

        //AREA DE TESTES
        if ($area == "test" || $area == "_Engine") {
            include(PROJECT_ROOT . DS . 'app' . DS . $area . DS . $controller);
        } else {
            $explode = explode("/", $url);
            $x = 0;
            foreach ($config['areas'] as $configAreas) {
                $x++;
                if ($configAreas == $area) {
                    if ($x == 1)
                        $nivel = 2 - 1;
                    else
                        $nivel = 2;
                }
            }
            if (!isset($nivel)) {
                $nivel = 2 - 1;
                $area = $config['areas'][0];
            }
            if (isset($explode[$nivel])) {
                if ($explode[$nivel] != null) {
                    $acao = $explode[$nivel];
                }
            }
            if ($explode[$nivel - 1] != null) {
                $controller = $explode[$nivel - 1];
            }

            $GLOBALS['area'] = $area;
            require_once(PROJECT_ROOT . DS . 'config' . DS . 'routes.php');

            $controllerNome = ucfirst($controller) . 'Controller';
            foreach (glob(PROJECT_ROOT . DS . 'app' . DS . 'modules' . DS . $area . DS . 'Controllers' . DS . '*') as $filename) {
                foreach (glob($filename . DS . "*") as $control) {
                    $expd = explode(DS, $control);
                    if ($controllerNome . ".php" == $expd[(count($expd) - 1)]) {
                        if (file_exists(PROJECT_ROOT . DS . 'app' . DS . 'modules' . DS . $area . DS . 'Controllers' . DS . $expd[(count($expd) - 2)] . DS . $controllerNome . ".php")) {
                            include(PROJECT_ROOT . DS . 'app' . DS . 'modules' . DS . $area . DS . 'Controllers' . DS . $expd[(count($expd) - 2)] . DS . $controllerNome . ".php");
                            $controller = $expd[(count($expd) - 2)];
                            define('_AREA', $area);
                            define('_CONTROLLER', $controller);
                            define('_ACTION', $acao);
                            $dispatch = new $controllerNome($area, $controller, basename($acao, ".php"));
                            $prefixo = 'post';
                            if ($_POST == null) {
                                $prefixo = "get";
                            }

                            if ((int)method_exists($controllerNome, $prefixo . $acao)) {
                                $metodo = $prefixo . $acao;
                            } else {
                                $metodo = $acao;
                            }

                            if ((int)method_exists($controllerNome, $metodo)) {
                                if (count($explode) == $nivel + 3) {
                                    $_GET['id'] = $explode[$nivel + 1];
                                }
                                if (!strstr($acao, "Grid")) {
                                    if ((int)method_exists($controllerNome, "beforeAction"))
                                        call_user_func([$dispatch, "beforeAction"]);
                                }
                                $GLOBALS['_pagina'] = call_user_func([$dispatch, $metodo]);
                                if (!strstr($acao, "Grid")) {
                                    if ((int)method_exists($controllerNome, "afterAction"))
                                        call_user_func([$dispatch, "afterAction"]);
                                }
                            } else {
                                if (count($explode) == $nivel + 2) {
                                    $_GET['id'] = $explode[$nivel];
                                }
                                if ((int)method_exists($controllerNome, "Escape")) {
                                    if ((int)method_exists($controllerNome, "beforeAction"))
                                        call_user_func([$dispatch, "beforeAction"]);
                                    $GLOBALS['_pagina'] = call_user_func([$dispatch, "Escape"]);
                                    if ((int)method_exists($controllerNome, "afterAction"))
                                        call_user_func([$dispatch, "afterAction"]);
                                } else {
                                    /* Erro 404*/
                                    echo "Conteudo não existente";
                                }

                            }
                        }
                    }
                }
            }


        }
    }
}





