<?php
namespace Havit\Library;


class Autoload
{
    public static function init()
    {
        @ini_set('default_charset', 'ISO-8859-1');
        define('DS', DIRECTORY_SEPARATOR);
        define('ROOT', dirname(dirname(dirname(dirname(dirname(dirname(dirname(__FILE__))))))));
        define('PROJECT_ROOT', dirname(dirname(dirname(dirname(dirname(dirname(__FILE__)))))));
        define('VENDOR', PROJECT_ROOT . DS . 'library');
        define('ENGINE', PROJECT_ROOT . DS . 'vendor' . DS . 'havit' . DS . 'imperium' . DS . 'src');

        setlocale(LC_ALL, 'pt_BR', 'pt_BR . iso - 8859 - 1', 'pt_BR . utf - 8', 'portuguese');

        date_default_timezone_set('America/Sao_Paulo');

        if (!empty($_GET['url'])) {
            $GLOBALS['url'] = $_GET['url'];
        }
        require_once PROJECT_ROOT . DS . 'config' . DS . 'config.php';

        /**
         * User: iurigustavo
         */
        setlocale(LC_ALL, 'ptb');
        require_once(ENGINE . DS . 'Library' . DS . 'engine.inc.php');
        require_once(ENGINE . DS . 'Library' . DS . 'base.inc.php');


        Engine::setRelatoriosErrors();
        Engine::removeMagicQuotes();
        Engine::callHook();
    }
}