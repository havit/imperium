<?php
    /**
     * Created by PhpStorm.
     * User: iuriasdf
     */
    $_now = new DateTime();
    $_now->setTimezone(new DateTimeZone("America/Porto_Velho"));
    $pageURL = 'http';
    if (isset($_SERVER["HTTPS"])) {
        if ($_SERVER["HTTPS"] == "on") {
            $pageURL .= "s";
        }
    }
    $pageURL .= "://";
    if ($_SERVER["SERVER_PORT"] != "80") {
        $pageURL .= $_SERVER["SERVER_NAME"] . ":" . $_SERVER["SERVER_PORT"] . $_SERVER["REQUEST_URI"];
    } else {
        $pageURL .= $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"];
    }
    foreach ($GLOBALS['conteudo']['conteudos'][$GLOBALS['app']['ambiente']] as $chave => $conteudo) {
        DEFINE($chave, $conteudo);
    }


        define('DATAHORA', $_now->format('d/m/Y H:i'));
    //    define('DATAHORA2', $_now->format('d-m-Y-H-i'));
    //    define('DATA', $_now->format('d/m/Y'));
    //    define('ANO', $_now->format('Y'));
    //    define('CONTENT', BASE_PATH . "resources/");
    //    define('CONTENT_GLOBAL', CONTENT . "global/");
    //    define('CONTENT_ADMIN', CONTENT . "admin/");
    //    define('CONTENT_VENDOR', CONTENT . "vendor/");
    //    define('CONTENT_SITE', CONTENT . "site/");
    //    define('CONTENT_UPLOAD', CONTENT . "upload/");
    define('CURRENT_URL', $pageURL);

