<?php
    //    session_start();
    /**
     * Class Conexao
     */
    namespace Havit\Core;

    use Exception;
    use PDO;
    use PDOException;
    use PDOStatement;

    class Conexao extends \PDO
    {

        // Classe de Conex?o
        /**
         * Cria uma inst?ncia
         * @var object Inst?cia
         */
        private static $instancia;

        private $sql = "";


        /**
         * ResultaSet
         * @var PDOStatement $res
         */
        var $res;
        /**
         * Linha (row) de uma consulta
         * @var object Registro de 1 linha
         */
        var $linha;
        /**
         * Quantidade de registros retornados
         * @var int Quantidade de Registros
         */
        var $qtd;
        /**
         * Statment da Consultas
         * @var PDOStatement $stmt
         */
        var $stmt;
        /**
         * Quando utilizado o INSERT ele retorna o valor da PK cadastrada no banco.
         * @var int Retorna ?ltima Pk cadastrada no banco.
         */
        var $last_id;
        /**
         * Lista de Parametros para o Prepared Statment
         * @var array
         */
        private $parametros = [];
        private $parametrostemp;
        private $where      = [];


        /**
         *
         */
        private function __clone()
        {
        }

        /**
         * Ao pegar a inst?ncia, ele retorna a conex?o
         *
         * @return Conexao
         */
        public static function getInstance()
        {


            // Se o a instancia n?o existe eu fa?o uma
            if (!isset(self::$instancia)) {
                try {

                    $database = $GLOBALS['database'];
                    $default = $GLOBALS['database']['default'];
                    $db = $database[$GLOBALS['app']['ambiente']][$default];
                    $options = [
                        Conexao::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES latin1',
                    ];

                    self::$instancia = new Conexao("mysql:host=" . $db['host'] . ';dbname=' . $db['database'], $db['usuario'], $db['senha'], $options);
                    self::$instancia->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
                    self::$instancia->setAttribute(\PDO::ATTR_DEFAULT_FETCH_MODE, \PDO::FETCH_BOTH);


                } catch (\Exception $e) {
                    echo 'Erro ao conectar <br />';
                    echo $e->getMessage();
                    exit();
                }
            } else {
            }

            // Se j? existe instancia na mem?ria eu retorno ela
            return self::$instancia;
        }

        /**
         * Adiciona parametros para a prepared statment, ir? substituir o ? em ordem.
         *
         * @param $parametro
         */
        public function adicionaParametros($parametro)
        {
            array_push($this->parametros, $parametro);
        }

        /**
         * Prepara o Statement
         *
         * @param string $sql
         */
        public function preparaStatement($sql)
        {
            $this->sql = $sql;
            if ($this->sql == "") { // Se nao foi passada nenhuma SQL,
                $this->res = 0; // Sem resultados
                $this->qtd = 0; // Sem linhas
            }
            $this->parametros = [];
            $this->stmt = self::prepare($this->sql);
        }

        /**
         * Executa a Statment
         * @return bool
         */
        public function executaStatement()
        {
            try {
                $x = 0;
                foreach ($this->parametros as $p) {
                    $x++;
                    $this->stmt->bindValue($x, $p);
                }

                $this->stmt->execute();
                $result = $this->stmt;
                $this->last_id = self::lastInsertId();
                $this->qtd = $result->rowCount();

                // error_log("PAGINA: " . $GLOBALS['url'] . " SQL: " . $this->interpolateQuery($this->sql, $this->parametros), 0);
                $this->sql = null;
                $this->parametros = [];
                $this->where = null;
                $this->parametrostemp = null;
                //                $this->tabela = null;

                if ($result) {
                    $this->res = $result;

                    return true;
                } else {
                    return false;
                }
            } catch (PDOException $e) {
                echo $e;
            }

            return false;

        }


        /**
         * @param string $sql
         * @param mixed  $parametros
         *
         * @return bool
         */
        public function executaConsulta($sql, $parametros = null)
        {
            $this->sql = $sql;
            $this->preparaStatement($sql);
            if (is_array($parametros)) {
                foreach ($parametros as $va) {
                    $this->adicionaParametros($va);
                }
            } else {
                if (isset($parametros)) {
                    $this->adicionaParametros($parametros);
                }
            }

            return $this->executaStatement();
        }

        /**
         * Resultado de uma consulta
         * @var mixed $class
         * @return bool
         */
        public function Resultado()
        {
            // Busca os dados de uma linha do resultado e
            // posiciona o ponteiro na proxima.
            if ($this->res) {
                $this->linha = $this->res->fetch();
            }
            if (!$this->linha) {
                return false;
            } else {
                return true;
            }
        }

        /**
         * @param $class
         *
         * @return mixed
         */
        public function getClass($class)
        {
            try {
                $this->res->setFetchMode(\PDO::FETCH_CLASS, $class);

                return $this->res->fetch();
            } catch (Exception $e) {
                error_log("Falha: " . $e->getMessage() . " URL: " . $GLOBALS['url'], 0);
            }

            return false;

        }

        /**
         * @param $class
         *
         * @return array
         */
        public function retornaArray($class = null)
        {
            if (isset($class)) {
                try {
                    return $this->res->fetchAll(PDO::FETCH_CLASS, "App\\DAO\\" . $class);
                } catch (Exception $e) {
                    error_log("Falha: " . $e->getMessage() . " URL: " . $GLOBALS['url'], 0);
                }
            } else {
                return $this->res->fetchAll(PDO::FETCH_ASSOC);
            }

            return false;
        }


    }

    ?>